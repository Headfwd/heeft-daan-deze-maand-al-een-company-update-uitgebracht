import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Lottie from 'react-lottie';

import { getVideos } from 'ducks/videos';
import { didHeDoItSelector, isGettingVideosSelector} from 'selectors/videos';

import { Container, Title, ConfettiWrapper, SadWrapper } from './styled';
import confettiAnimation from './confettiAnimation.json';
import sadAnimation from './sadAnimation.json';

const Main: React.FC = () => {
  const dispatch = useDispatch();

  const didHeDoIt: Boolean = useSelector(didHeDoItSelector);
  const isGettingVideos: Boolean = useSelector(isGettingVideosSelector);

  React.useEffect(() => {
    dispatch(getVideos());
  }, []);

  if (isGettingVideos) return null;

  return (
    <Container>
      <Title>Heeft daan deze maand al een company update uitgebracht?</Title>
      {didHeDoIt ? (
        <>
          <Title>YESSS!!!</Title>
          <ConfettiWrapper>
              <Lottie
                options={{
                  loop: true,
                  animationData: confettiAnimation,
                  rendererSettings: {
                    preserveAspectRatio: 'xMidYMid slice',
                  },
                }}
              />
          </ConfettiWrapper>
        </>
      ) : (
        <SadWrapper>
            <Lottie
              options={{
                loop: true,
                animationData: sadAnimation,
                rendererSettings: {
                  preserveAspectRatio: 'xMidYMid slice',
                },
              }}
            />
        </SadWrapper>
      )}
    </Container>
  );
};

export default Main;
