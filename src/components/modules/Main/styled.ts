import styled from 'styled-components';

export const Container = styled.div`
  background-color: ${({ theme }) => theme.colors.white};
  width: 100vw;
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  padding: 40px;
`;

export const Title = styled.h1`
  font-size: 80px;
  fon-family: 'arial';
  text-align: center;
`;

export const ConfettiWrapper = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
`;

export const SadWrapper = styled.div`
  width: 220px;
  background-color: #FFCC4E;
  border-radius: 100%;
  padding: 10px;
`;
