import React, { lazy, Suspense } from 'react';
import { Switch, Route, withRouter, RouteComponentProps } from 'react-router-dom';

import GlobalStyle from 'styles';

const Main = lazy(() => import('modules/Main'));

const App: React.FC<RouteComponentProps> = () => (
  <main>
    <GlobalStyle />
    <Suspense fallback={null}>
      <Switch>
        <Route path="/" component={Main} exact />
      </Switch>
    </Suspense>
  </main>
);

export default withRouter(App);
