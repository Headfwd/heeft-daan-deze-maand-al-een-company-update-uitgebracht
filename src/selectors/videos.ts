import { createSelector } from 'reselect';
import dayjs from 'dayjs';

import { Video } from 'types';

const regex = new RegExp(/(.*)(company)(.*)(update)/g);

export const videosSelector = createSelector(
  (state: { videos: { data: Video[] } }) => state.videos.data,
  (videos: Video[]) =>
    videos
      .map((video: Video) => ({
        id: video.id,
        status: video.status,
        name: video.name,
        created_time: video.created_time,
      }))
      .filter((video) => regex.test(video.name.toLowerCase())),
);

export const isGettingVideosSelector = (state: { videos: { loading: Boolean } }) => state.videos.loading;

export const didHeDoItSelector = createSelector(
  videosSelector,
  (videos) => videos.some((video) => dayjs().isSame(dayjs(video.created_time), 'month')),
);
