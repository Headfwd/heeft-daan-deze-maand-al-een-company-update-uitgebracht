import 'styled-components';

const theme = {
  colors: {
    white: '#FFFFFF',
    black: '#000000',
  },
} as const;

export default theme;
