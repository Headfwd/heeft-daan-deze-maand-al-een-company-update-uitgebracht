/**
 * This is an example file from react-prime
 */
import * as i from 'types';
import { ActionType, action } from 'typesafe-actions';
import { DataState, Video } from './types';

export const dataActions = {
  request: () => action('videos/GET_REQUEST'),
  success: (video: Video[]) => action('videos/GET_SUCCESS', video),
  failure: (error: Error) => action('videos/GET_FAILURE', error),
};

const initialState: DataState = {
  data: [],
  error: false,
  loading: false,
};

export default (state = initialState, action: ActionType<typeof dataActions>) => {
  switch (action.type) {
    case 'videos/GET_REQUEST':
      return {
        ...state,
        error: false,
        loading: true,
      };
    case 'videos/GET_SUCCESS':
      return {
        ...state,
        data: action.payload,
        error: false,
        loading: false,
      };
    case 'videos/GET_FAILURE':
      return {
        ...state,
        loading: false,
        error: action.payload.message,
      };
    default:
      return state;
  }
};

export const getVideos: i.BaseThunkAction<() => void>['thunk'] = () => (dispatch) => {
  dispatch(dataActions.request());

  fetch('https://api.vimeo.com/users/374114059/videos?sort=date&order=desc')
    .then((response: Response) => {
      if (!response.ok) {
        throw new Error(response.statusText);
      }
      return response.json();
    })
    .then((videos: Video[]) => {
      dispatch(dataActions.success(videos))
    })
    .catch((error: Error) => {
      dispatch(dataActions.failure(error));
    });
};
