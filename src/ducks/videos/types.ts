import * as i from 'types';

export type DataState = {
  data: Video[];
  error: boolean;
  loading: boolean;
};

export type GetData = i.BaseThunkAction<() => void>;

export type Video = {
  id: string;
  status: 'available' | 'quota_exceeded' | 'total_cap_exceeded' | 'transcode_starting' | 'transcoding' | 'transcoding_error' | 'unavailable' | 'uploading' | 'uploading';
  created_time: string;
  name: string;
};
