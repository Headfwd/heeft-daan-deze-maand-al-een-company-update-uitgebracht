import * as i from 'types';
export * from './videos/types';

export type ReduxState = {
  data: i.DataState;
};
